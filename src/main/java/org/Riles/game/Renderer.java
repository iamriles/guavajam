package org.Riles.game;

import org.Riles.engine.graph.ShaderProgram;
import org.Riles.engine.graph.Mesh;
import org.Riles.engine.Utils;
import org.Riles.engine.Window;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import java.nio.FloatBuffer;
import org.lwjgl.system.MemoryUtil;
import org.joml.Matrix4f;
import org.joml.Math.*;

public class Renderer {

    private int vaoId;
    private int vboId;

    private ShaderProgram shaderProgram;


    // projection constants
    private static final float ZNEAR = 0.01f;
    private static final float ZFAR = 1000.0f;
    private static final float FIELDOFVIEW =
        (float) Math.toRadians(60.0f);

    private Matrix4f projectionMatrix;

    public Renderer() {
    }

    /**
     * finds and loads the shaders into the
     * ShaderProgram
     **/
    public void init(Window window) throws Exception {

        // set up shaders
        shaderProgram = new ShaderProgram();

        shaderProgram
            .createVertexShader(Utils.loadResource("shaders/vertex.glsl"));

        shaderProgram
            .createFragmentShader(Utils.loadResource("shaders/fragment.glsl"));

        float aspectRatio = (float) window.getWidth() / window.getHeight();

        projectionMatrix = new Matrix4f()
            .perspective(FIELDOFVIEW,
                         aspectRatio,
                         ZNEAR,
                         ZFAR);

        shaderProgram.link();

        // link shader program first!
        shaderProgram.createUniform("projectionMatrix");
    }

    /**
     * render the frame to the given GLFW window
     * @param window used to the renderer which window to render to
     * @param mesh vertex data that will be renderered
     **/
    public void render(Window window, Mesh[] meshes) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());

            float aspectRatio = (float) window.getWidth() / window.getHeight();

            projectionMatrix = new Matrix4f()
                .perspective(FIELDOFVIEW,
                             aspectRatio,
                             ZNEAR,
                             ZFAR);

            window.setResized(false);
        }



        shaderProgram.bind();

        shaderProgram.setUniform("projectionMatrix", projectionMatrix);

        for (Mesh mesh : meshes){
            mesh.render();
        }

        shaderProgram.unbind();
    }

    /**
     * This method frees the shaderProgram from memory
     * and does standard cleanup so the program will exit
     * as expected.
     **/
    public void cleanup() {
        if (shaderProgram != null) {
            shaderProgram.cleanup();
        }
    }

    /**
     * Calls OpenGl's clear to set the background color of the frame
     * to what ever value the GL_COLOR_BUFFER_BIT is set to.
     **/
    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}
