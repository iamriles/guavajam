package org.Riles.game;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.opengl.GL11.glViewport;
import org.Riles.engine.graph.Mesh;
import org.Riles.engine.IGameLogic;
import org.Riles.engine.Window;

public class DummyGame implements IGameLogic {

    private boolean direction = true;
    private float color = 0.0f;
    private float rate = 1.0f;
    private Mesh[] meshes;

    private final Renderer renderer;

    public DummyGame() {
        renderer = new Renderer();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        // Code for square
        float[] vertPosition1 = new float[]{
            -0.3f, 0.3f, -3.5f,
            -0.3f, -0.3f, -1.5f,
            0.3f, -0.3f, -1.5f,
            0.3f, 0.3f, -1.5f
        };

        int [] vertIndeces1 = new int[]{
            0, 1, 3, 3, 1, 2
        };

        float [] vertColors1 = new float[]{
            0.0f, 0.0f, 1.0f,
            0.0f, 1.0f, 1.0f,
            1.0f, 0.5f, 0.0f,
            1.0f, 0.0f, 0.0f,
        };

        // Code for square2
        float[] vertPosition2 = new float[]{
            -0.8f, 0.3f, -3.5f,
            -0.8f, -0.3f, -1.5f,
            -0.2f, -0.3f, -1.5f,
            -0.2f, 0.3f, -1.5f
        };

        int [] vertIndeces2 = new int[]{
            0, 1, 3, 3, 1, 2
        };

        float [] vertColors2 = new float[]{
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
        };


        Mesh mesh1 = new Mesh(vertPosition1, vertIndeces1, vertColors1);
        Mesh mesh2 = new Mesh(vertPosition2, vertIndeces2, vertColors2);
        meshes = new Mesh[]{mesh1, mesh2};
    }

    @Override
    public void input(Window window) {
        if(window.isKeyPressed(GLFW_KEY_UP) && rate <= 5.0f){
            rate += 0.1;
        } else if (window.isKeyPressed(GLFW_KEY_DOWN) && rate >= 0.2f) {
            rate -= 0.1;
        }
    }

    //update game state
    @Override
    public void update(float interval) {
        if (direction) {
            color += 0.01f * rate;
        } else {
            color -= 0.01f * rate;
        }

        if (color > 1) {
            color = 1.0f;
            direction = false;
            System.out.println("going down");
        } else if ( color < 0 ) {
            color = 0.0f;
            direction = true;
            System.out.println("going up");
        }
    }

    @Override
    public void render(Window window) {
        window.setClearColor(color, color, color, 0.0f);
        renderer.render(window, meshes);
    }

    @Override
    public void cleanup () {
    }
}
