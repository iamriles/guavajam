/**
 * Mesh Class
 * ==================================
 * This is a simple class to handle
 * Vertex array objects and make
 * them easier to work with.
 **/

package org.Riles.engine.graph;

// <IMPORTS>
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import org.lwjgl.system.MemoryUtil;

public class Mesh {
    //Vertex array object
    private final int vaoId;

    //Vertex buffer objects
    private final int positionsVboId;
    private final int indicesVboId;
    private final int colorsVboId;

    private final int vertexCount;

    public Mesh(float[] vertexPositions, int[] vertexIndices, float[] vertexColors) {

        //setup buffers
        FloatBuffer positionsBuffer = null;
        IntBuffer indicesBuffer = null;
        FloatBuffer colorsBuffer = null;

        try {
            vertexCount = vertexIndices.length;

            vaoId = glGenVertexArrays();
            glBindVertexArray(vaoId);

            // <NOTE> Possible refactor here
            // the code below is a bit repetitive and it maybe possible
            // to clean it up and simplify it using closures or lambdas
            // (what ever they're called in java)

            // POSITIONS BUFFER SETUP
            positionsVboId = glGenBuffers();
            positionsBuffer = MemoryUtil.memAllocFloat(vertexPositions.length);
            positionsBuffer.put(vertexPositions).flip();
            glBindBuffer(GL_ARRAY_BUFFER, positionsVboId);
            glBufferData(GL_ARRAY_BUFFER, positionsBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

            // INDICES BUFFER SETUP
            indicesVboId = glGenBuffers();
            indicesBuffer = MemoryUtil.memAllocInt(vertexIndices.length);
            indicesBuffer.put(vertexIndices).flip();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboId);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);

            // COLOR BUFFER SETUP
            colorsVboId = glGenBuffers();
            colorsBuffer = MemoryUtil.memAllocFloat(vertexColors.length);
            colorsBuffer.put(vertexColors).flip();
            glBindBuffer(GL_ARRAY_BUFFER, colorsVboId);
            glBufferData(GL_ARRAY_BUFFER, colorsBuffer, GL_STATIC_DRAW);

            // first arguments is changed to 1
            glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);

        } finally {
            if (positionsBuffer != null){
                MemoryUtil.memFree(positionsBuffer);
            }
            if (positionsBuffer != null){
                MemoryUtil.memFree(indicesBuffer);
            }
            if (colorsBuffer != null){
                MemoryUtil.memFree(colorsBuffer);
            }
        }
    }

    public void render(){
        // bind the VAO
        glBindVertexArray(getVaoId());

        // Make sure the vertex arrays are good to go
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        //actually draw the mesh
        glDrawElements(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0);

        // restore state
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    /**
     * getter for the Id of the Vertex Array
     * @return Id of the Vertex Array
     **/
    public int getVaoId() {
        return vaoId;
    }

    /**
     * getter for the total number of verteces
     * @return number of verteces in the mesh
     **/
    public int getVertexCount(){
        return vertexCount;
    }

    /**
     * cleans up the memory used by Opengl
     * that is related to the mesh.
     **/
    public void cleanUp() {
        glDisableVertexAttribArray(0);

        // free up the Vertex buffer
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionsVboId);
        glDeleteBuffers(indicesVboId);
        glDeleteBuffers(colorsVboId);

        // free up the actual vertex array
        glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
    }

}//end of Mesh class
