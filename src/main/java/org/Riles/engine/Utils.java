package org.Riles.engine;

import java.util.Scanner;
import java.io.File;

public class Utils {
    public static String loadResource(String fileName) throws Exception {

        String result =
            new Scanner (new File(fileName))
            .useDelimiter("\\Z").next();

        return result;
    }
}
