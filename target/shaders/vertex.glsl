#version 330

// the first parameter of glAttrib function is the the location
// that is being used here
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inputColor;

// Color output value that will just be passed to the fragment
// shader
out vec3 outputColor;
uniform mat4 projectionMatrix;

void main() {
  gl_Position = projectionMatrix * vec4(position, 1.0);

  // just pass it to the fragment shader
  outputColor = inputColor;
}
