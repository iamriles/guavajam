#version 330

out vec4 fragColor;
uniform float u_time;

void main(){
  fragColor = vec4(abs(sin(u_time)), 0.5, 0.5, 1.0);
}
